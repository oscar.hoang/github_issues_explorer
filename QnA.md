# QnA

### Planning

> What do you think are the greatest areas of risk in completing the project?

- How to query the github repo url 1. as the user types (mockup) or 2. with a button on submit
- How to display the next set of issues to the user e.g. Pagination
- How to clear the current query results when clicking X (provided icon)

> What changes/additions would you make to the design?

- Add Design for implementation of Pagination feature
- Add Design for implementation on 'selected issue' from list of repo issues
- Add Design: EmptyState, Loading, and Error States
- Add Design: Search Button to begin query instead of auto fetching

> List a two or three features that you would consider implementing in the future that would add significant value to the project.

- Improve the UX by having a list of 'most popular' repos made available on App start
- Improve the UX by having a list of 'successful (200) queries' saved to Local Storage whenver a user refreshes the page
- After successfully (200) found issues for a repo - being able to continue editing search query (header bar).

---

### Looking Back

> Describe the major design/build decisions and why you made them.

- Use familiar technology stack/framework to save time: Create-React-App, TailwindCSS, React-Router-Dom, Axios and etc.
- Would have liked to apply Apollo or CSS Modules. But due to time constraints it was not possible to learn new technologies with the recommented time to complete
- I choose to follow the design mockups exactly. Because as a Developer before starting on the task/assignement I'm usually given:

1. The Design and
2. The Task

This is all I need to start on any task/assignment.
And if there is any uncertainty, I will ask the product manager for clarification or use it as an opportunity for creative freedom

> How long did the assignment take (in hours)? Please break down your answer into buckets (e.g. "Learning Framework", "Coding", "Debugging").

In my experience with past take-home assignments, they usually take much longer than the proposed project estimate(hrs).
This assignment took me 19 HRS to complete
- 16 hrs 30 mins	Coding/Debugging
- 1 hr 30 mins	  Project Setup
- 1 hr            Planning

> If you could go back and give yourself advice at the beginning of the project, what would it be?

- Have my Local (Computer) Environment setup for 'multiple' GitHub Accounts e.g. commit, push, pull, merge, and etc.

> Did you learn anything new?

- Something I learned with this assginment, was using the GitHub Api.
- I was also able to test my Code and the API with new tools found through VSCode Extensions: Quokka and Thunder Client

> Do you feel that this assignment allowed you to showcase your abilities effectively?

The assignment allowed me to showcase:
- How I layout, organize and structure my code
- How I utilize different React Libraries: create-react-app, react-router-dom, tailwindCSS
- How I pass props and use state
- Functional components, Hooks, custom Hooks
- ES6

> Are there any significant web development-related skills that you possess that were not demonstrated in this exercise? If so, what are they?

I am able to wear many hats and am able to work in most areas (front and back) of modern web application.
I have a background in web design (HTML/CSS, Photoshop, Sketch, and Figma) and development (React/JS) to build web-apps (MERN Stack).

This (timeboxing) assignment showcased my skills and knowledge in retriving data from an api endpoint.
If there was different assignment that required another set of features/functionality.
As a (front-end) developer, I am confident, I would possess the skills to complete it as well.
