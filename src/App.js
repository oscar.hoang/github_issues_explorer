import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import IssueSearchView from './components/IssueSearchView';
import NotFound from './components/NotFound';

const App = () => (
  <Router>
    <Switch>
      <Route path='/' exact component={IssueSearchView} />
      <Route component={NotFound} />
    </Switch>
  </Router>
);

export default App;
