import React from 'react';
import PropTypes from 'prop-types';
import PageLayout from './layout/PageLayout';

const ErrorMessage = ({ message }) => (
  <PageLayout>
    <div role='alert' className='flex mb-4 p-4 bg-red-400 rounded-lg text-white text-sm font-bold'>
      <p className='text-base'>{message}</p>
    </div>
  </PageLayout>
);

ErrorMessage.propTypes = {
  message: PropTypes.string
};

export default ErrorMessage;
