import React from 'react';

import useIssueSearch from '../hooks/useIssueSearch';
import useQueryDebounce from '../hooks/useQueryDebounce';

import PageOverlay from './layout/PageOverlay';
import SearchInput from './SearchInput';
import IssueSelectView from './IssueSelectView/IssueSelectView';
import ErrorMessage from './ErrorMessage';
import PageLayout from './layout/PageLayout';
import PageNavigation from './IssueSelectView/PageNavigation';

const IssueSearch = () => {
  const [query, debouncedChangeHandler, clearQuery] = useQueryDebounce();

  const [
    data,
    isLoading,
    error,
    filterBy,
    page,
    maxPage,
    clearIssueSearchState,
    filterChangeHandler,
    prevPage,
    nextPage
  ] = useIssueSearch(query);

  const clearState = () => {
    clearQuery();
    clearIssueSearchState();
  };

  const hasIssues = Array.isArray(data?.issues) && data?.issues?.length > 0;

  return (
    <React.Fragment>
      {!hasIssues && isLoading && <PageOverlay />}

      {!hasIssues && (
        <div className='flex items-center justify-center h-screen'>
          <PageLayout>
            <div className='flex flex-col gap-y-4'>
              <SearchInput debouncedChangeHandler={debouncedChangeHandler} />
              {error && (
                <ErrorMessage message={error.message} />
              )}
              {data?.code === 200 && (
                <div className='mt-10'>
                  <p className='text-center'>Found no Issues for {query}</p>
                </div>
              )}
            </div>
          </PageLayout>
        </div>
      )}

      {hasIssues && (
        <IssueSelectView
          query={query}
          issues={data?.issues}
          filterBy={filterBy}
          isLoading={isLoading}
          filterChangeHandler={filterChangeHandler}
          clearState={clearState}
        >
          <PageNavigation
            isLoading={isLoading}
            page={page}
            maxPage={maxPage}
            prevPageChangeHandler={prevPage}
            nextPageChangeHandler={nextPage}
          />
        </IssueSelectView>
      )}
    </React.Fragment>
  );
};

export default IssueSearch;
