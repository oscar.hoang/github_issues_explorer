import React from 'react';
import PropTypes from 'prop-types';

import PageLayout from '../layout/PageLayout';

const IssueHeader = ({ query }) => (
  <div className='py-10 border-b-2'>
    <PageLayout>
      <div className='grid grid-cols-1 md:grid-cols-2 items-center justify-between'>
        <h1 className='text-2xl font-bold'>GitHub Issue Viewer</h1>
        <div className='md:ml-auto'>
          <p className='text-base text-gray-500'>{query}</p>
        </div>
      </div>
    </PageLayout>
  </div>
);

IssueHeader.propTypes = {
  query: PropTypes.string
};

export default IssueHeader;
