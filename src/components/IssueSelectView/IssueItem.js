import React from 'react';
import PropTypes from 'prop-types';

import PRIcon from '../../assets/icons/pull-request.svg';
import IssueClosedIcon from '../../assets/icons/issue-closed.svg';
import IssueItemSkeleton from './IssueItemSkeleton';

const IssueItemLabels = ({ labels }) => (
  <div className='flex flex-wrap items-center gap-2'>
    {labels.map((label) => (
      <div
        key={label.id}
        className='px-2 py-1 rounded text-xs border-2 border-black'
      >
        <span className='text-black'>{label.name}</span>
      </div>
    ))}
  </div>
);

IssueItemLabels.propTypes = {
  labels: PropTypes.array
};

const IssueIconLayout = ({ icon, name }) => (
  <div className='flex flex-grow w-6 h-6'>
    <img src={icon} alt={name} />
  </div>
);

IssueIconLayout.propTypes = {
  icon: PropTypes.any,
  name: PropTypes.string
};

const IssueItem = ({ issue, filterBy, isLoading }) => {
  const isClosed = issue && issue.closed_at;
  const isPR = (issue && issue.pull_request) || filterBy === 'pulls';

  if (isLoading) return <IssueItemSkeleton />;

  return (
    <a
      href={issue.html_url}
      target='_blank'
      rel='noopener noreferrer'
      className='flex flex-col p-4 rounded-lg border-2 shadow-md'
    >
      <div className='mb-20'>
        <div className='flex align-center justify-between mb-10 gap-x-2'>
          <p className='text-lg font-bold break-normal truncate'>{issue.title}</p>
          <div className='flex gap-x-2'>
            {isPR && (
              <IssueIconLayout icon={PRIcon} name='pull-request' />
            )}
            {isClosed && (
              <IssueIconLayout icon={IssueClosedIcon} name='issue-closed' />
            )}
          </div>
        </div>
        <p className='text-base text-gray-700 break-normal truncate'>{issue.body}</p>
      </div>
      <IssueItemLabels labels={issue.labels} />
    </a>
  );
};

IssueItem.propTypes = {
  issue: PropTypes.object,
  filterBy: PropTypes.string,
  isLoading: PropTypes.bool
};

export default IssueItem;
