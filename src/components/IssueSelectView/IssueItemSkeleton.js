import React from 'react';

const IssueItemSkeleton = () => (
  <div className='max-h-80 p-4 bg-white rounded-lg border-2 shadow-md animate-pulse'>
    <div className='mb-20'>
      <div className='flex align-center justify-between mb-10'>
        <div className='h-4 bg-gray-400 rounded w-full'></div>
      </div>
      <div className='h-4 bg-gray-400 rounded w-full'></div>
    </div>
    <div className='flex items-center gap-2'>
      <div className='h-4 bg-gray-400 rounded w-1/3'></div>
      <div className='h-4 bg-gray-400 rounded w-1/3'></div>
      <div className='h-4 bg-gray-400 rounded w-1/3'></div>
    </div>
  </div>
);

export default IssueItemSkeleton;
