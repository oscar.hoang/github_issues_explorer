import React from 'react';
import PropTypes from 'prop-types';

import IssueItem from './IssueItem';
import PageLayout from '../layout/PageLayout';

const IssueItems = ({ issues, filterBy, isLoading }) => (
  <PageLayout>
    <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4'>
      {issues.map((issue) => <IssueItem key={issue.id} issue={issue} filterBy={filterBy} isLoading={isLoading} />)}
    </div>
  </PageLayout>
);

IssueItems.propTypes = {
  issues: PropTypes.array,
  filterBy: PropTypes.string,
  isLoading: PropTypes.bool,
};

export default IssueItems;
