import React from 'react';
import PropTypes from 'prop-types';

import IssueHeader from './IssueHeader';
import IssueItems from './IssueItems';
import IssueNav from './nav/IssueNav';

const IssueSelectView = ({
  query,
  issues,
  children,
  filterBy,
  isLoading,
  filterChangeHandler,
  clearState
}) => {
  return (
    <div className='flex flex-col gap-y-10 mb-20'>
      <IssueHeader query={query} />
      <IssueNav
        filterBy={filterBy}
        filterChangeHandler={filterChangeHandler}
        clearState={clearState}
      />
      {children}
      <IssueItems
        issues={issues}
        filterBy={filterBy}
        isLoading={isLoading}
      />
    </div>
  );
};

IssueSelectView.propTypes = {
  query: PropTypes.string,
  issues: PropTypes.array,
  filterBy: PropTypes.string,
  isLoading: PropTypes.bool,
  filterChangeHandler: PropTypes.func,
  clearState: PropTypes.func
};

export default IssueSelectView;
