import React from 'react';
import PropTypes from 'prop-types';
import PageLayout from '../layout/PageLayout';

const PageNavigation = ({
  isLoading,
  page,
  maxPage,
  prevPageChangeHandler,
  nextPageChangeHandler
}) => {
  const buttonStyle = `
    py-1 px-4 bg-white border-2 rounded-md
  `;

  return (
    <PageLayout>
      <div className='flex gap-x-2'>
        <button
          type='button'
          disabled={isLoading || page === 1}
          className={`${buttonStyle} ${(isLoading || (page === 1)) && 'opacity-50 cursor-not-allowed'}`}
          onClick={() => prevPageChangeHandler()}
        >
          Prev
        </button>
        <button
          type='button'
          className={`${buttonStyle} ${(isLoading || (page >= maxPage)) && 'opacity-50 cursor-not-allowed'}`}
          disabled={isLoading || (page >= maxPage)}
          onClick={() => nextPageChangeHandler()}
        >
          Next
        </button>
      </div>
    </PageLayout>
  );
};

PageNavigation.propTypes = {
  isLoading: PropTypes.bool,
  page: PropTypes.number,
  maxPage: PropTypes.number,
  prevPageChangeHandler: PropTypes.func,
  nextPageChangeHandler: PropTypes.func
};

export default PageNavigation;
