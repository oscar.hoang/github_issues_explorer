import React from 'react';
import PropTypes from 'prop-types';

import IssueNavItems from './IssueNavItems';
import PageLayout from '../../layout/PageLayout';

import CloseIcon from '../../../assets/icons/close.svg';

const IssueNav = ({ filterBy, filterChangeHandler, clearState }) => (
  <PageLayout>
    <div className='flex flex-wrap items-center justify-between'>
      <IssueNavItems filterBy={filterBy} filterChangeHandler={filterChangeHandler} />
      <button type='button' className='w-10 h-10' onClick={() => clearState()}>
        <img src={CloseIcon} alt='close' />
      </button>
    </div>
  </PageLayout>
);

IssueNav.propTypes = {
  filterBy: PropTypes.string,
  filterChangeHandler: PropTypes.func,
  clearState: PropTypes.func
};

export default IssueNav;
