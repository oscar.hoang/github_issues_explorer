import React from 'react';
import PropTypes from 'prop-types';

const IssueNavItem = ({ item, title, isDisabled, filterChangeHandler }) => {
  const buttonStyle = `
    flex justify-start text-black font-bold
    text-xs uppercase outline-none
    focus:outline-none ease-linear
    transition-all duration-150
    ${isDisabled && 'text-gray-300 cursor-not-allowed'}
  `;

  return (
    <button
      type='button'
      className={buttonStyle}
      disabled={isDisabled}
      onClick={() => filterChangeHandler(item)}
    >
      {title}
    </button>
  );
};

IssueNavItem.propTypes = {
  item: PropTypes.string,
  title: PropTypes.string,
  isDisabled: PropTypes.bool,
  filterChangeHandler: PropTypes.func
};

export default IssueNavItem;
