import React from 'react';
import PropTypes from 'prop-types';

import IssueNavItem from './IssueNavItem';

const LINKS = ['all', 'open', 'closed', 'pulls'];

const IssueNavItems = ({ filterBy, filterChangeHandler }) => {
  const getLinkTitle = (type) => {
    switch (type) {
      case 'open':
        return 'Open Issues';
      case 'closed':
        return 'Closed Issues';
      case 'pulls':
        return 'Pull Requests';
      default:
        return 'All Issues';
    }
  };

  return (
    <div className='flex items-center justify-center gap-x-1 md:gap-x-8 mb-4 md:mb-0'>
      {LINKS.map((link) => {
        const title = getLinkTitle(link);
        const isDisabled = filterBy === link;
        return (
          <IssueNavItem
            key={link}
            item={link}
            title={title}
            isDisabled={isDisabled}
            filterChangeHandler={filterChangeHandler}
          />
        );
      })}
    </div>
  );
};

IssueNavItems.propTypes = {
  filterBy: PropTypes.string,
  filterChangeHandler: PropTypes.func
};

export default IssueNavItems;
