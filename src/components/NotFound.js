import React from 'react';
import { Link } from 'react-router-dom';

import PageLayout from './layout/PageLayout';

const NotFound = () => (
  <PageLayout>
    <div className='flex flex-col items-center justify-center h-screen'>
      <div className='mb-4'>
        <h1 className='text-xl font-bold'>Page Not Found</h1>
      </div>
      <Link to='/' className='py-1 px-4 bg-white border-2 rounded-md'>Go Back</Link>
    </div>
  </PageLayout>
);

export default NotFound;
