import React from 'react';
import PropTypes from 'prop-types';
import SearchIcon from '../assets/icons/search.svg';

const SearchInput = ({ debouncedChangeHandler }) => {
  const handleSubmit = (e) => e.preventDefault();

  return (
    <form onSubmit={handleSubmit} className='relative flex flex-wrap items-stretch w-full'>
      <span className='
        items-center justify-center w-8 pt-4 pb-2 pl-3 z-10 h-full
        absolute bg-transparent rounded-lg text-center leading-snug'
      >
        <img src={SearchIcon} alt='search' />
      </span>
      <input
        id='search'
        className='
          w-full pl-10 p-3 outline-none focus:outline-none
          focus:ring-2 focus:ring-black bg-white border-2 rounded-lg'
        type='text'
        name='q'
        placeholder='Paste a link to a GitHub repo e.g. https://www.github.com/facebook/react'
        autoFocus
        onChange={(e) => debouncedChangeHandler(e.target.value)} />
    </form>
  );
};

SearchInput.propTypes = {
  debouncedChangeHandler: PropTypes.func
};

export default SearchInput;
