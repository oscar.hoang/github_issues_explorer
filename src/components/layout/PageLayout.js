import React from 'react';
import PropTypes from 'prop-types';

const PageLayout = ({ children }) => (
  <div className='container md:mx-auto'>
    {children}
  </div>
);

PageLayout.propTypes = {
  children: PropTypes.element,
};

export default PageLayout;
