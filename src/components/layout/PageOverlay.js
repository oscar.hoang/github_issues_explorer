import React from 'react';

const PageOverlay = () => {
  return (
    <div className='animated fadeIn fixed z-50 pin overflow-auto bg-black flex w-full opacity-80 h-screen'>
      <div className='m-auto'>
        <div className='text-white'>Loading...</div>
      </div>
    </div>
  );
};

export default PageOverlay;
