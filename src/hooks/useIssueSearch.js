import { useState, useEffect } from 'react';
import search from '../services/github';

const useIssueSearch = (query) => {
  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [filterBy, setFilterBy] = useState('all');
  const [page, setPage] = useState(1);
  const [maxPage, setMaxPage] = useState(0);

  useEffect(() => {
    if (query !== '') {
      const loadData = async () => {
        try {
          setError(null);
          setIsLoading(true);
          const res = await search(query, filterBy, page);

          // Find Max Pages
          const { link } = res.headers || '';
          if (link) {
            const links = link.split(',');
            const urls = links.map((a) => {
              return parseInt(a.split(';')[0].replace('>', '').replace('<', '').split('&page=')[1], 10);
            }); // urls = [2 (next), 64 (last)]

            setMaxPage(urls[1]);
          }
          setData({
            code: res.status,
            issues: res.data
          });
        } catch (err) {
          setError(err?.response?.data);
        }
        setIsLoading(false);
      };
      loadData();
    }
  }, [query, filterBy, page]);

  const clearIssueSearchState = () => {
    setData(null);
    setError(null);
    setFilterBy('all');
    setPage(1);
    setMaxPage(0);
  };

  const filterChangeHandler = (type) => {
    setFilterBy(type);
    setPage(1);
  };

  const previousPage = () => setPage(Math.max(0, page - 1));
  const nextPage = () => setPage(page + 1);

  return [
    data,
    isLoading,
    error,
    filterBy,
    page,
    maxPage,
    clearIssueSearchState,
    filterChangeHandler,
    previousPage,
    nextPage
  ];
};

export default useIssueSearch;
