import axios from 'axios';

const search = (query, state, page) => {
  const { pathname } = new URL(query);
  const repo = pathname.split('/').filter((item) => item);
  const path = state !== 'pulls' ? `issues?state=${state}` : `pulls?state=all`;

  const res = axios.get(
    `https://api.github.com/repos/${repo[0]}/${repo[1]}/${path}&page=${page}`
  );
  return res;
};

export default search;
